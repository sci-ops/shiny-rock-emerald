---
title: "Intro"
output: html_document
---

# Welcome to this Shiny ROCK app!

This Shiny App was developed for easy playing around with the Qualitative Network Approach. It allows you to paste a ROCK source and will then parse it and produce a network from the codes.

This method is very new, and as yet has only been described in a brief preprint at [https://doi.org/hwzj](https://doi.org/hwzj).

The app assumes you followed the ROCK conventions, using double square brackets to denote coding; and that you separate the origin code from the target code with `->`, then use `||` to separate these from the edge type, and finally, optionally include another `||` to specify the edge weight. Aesthetic properties for the edges of different types can be specified using DOT attributes in a YAML segment.

This is an example of a valid source. To try out the app, you can copy-paste this text into the text area in the second tab.

